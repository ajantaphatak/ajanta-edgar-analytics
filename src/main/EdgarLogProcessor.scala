import java.io.{BufferedWriter, File, FileWriter}
import java.util.Date

import scala.annotation.tailrec
import scala.io.Source
import scala.util.Try

case class InputLine(ip: String, dt: Date, cik: String, accession: String, extention: String, order: Int)

case class Output(ip: String, start: Date, end: Date, numDocs: Int)

case class Result(ipMap: Map[String, Set[InputLine]], expireMap: Map[Long, List[String]])

object EdgarLogProcessor extends App {
  val dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  if (args.length < 3) {
    usage("Insufficient arguments")
    System.exit(1)
  }

  val inactivityPeriodFile = Try(Source.fromFile(args(1)))
  val expireVal = Try(inactivityPeriodFile.get.getLines().next.toInt)
  if (inactivityPeriodFile.isFailure || expireVal.isFailure) {
    usage("Error reading inactivity period file (2nd argument). Please make sure it exists, is readable and holds a single integer value denoting the period of inactivity (in seconds).")
    System.exit(1)
  }

  val expireTimeVal = expireVal.get

  // close the file but ignore if failure occurs
  Try(inactivityPeriodFile.get.close)

  val inputfile = Try(Source.fromFile(args(0)))
  if (inputfile.isFailure) {
    usage("Error reading input log file (1st argument). Please make sure it exists and is readable.")
    System.exit(1)
  }
  val inputLogFile = inputfile.get

  val outFile = new File(args(2))

  // set the expire time appropriately
  val expireMillis = expireTimeVal * 1000
  val outputfw = Try(new BufferedWriter(new FileWriter(outFile)))
  if (outputfw.isFailure) {
    usage("Error creating output file (3rd argument). Please make sure the path to the file exists and is writable.")
    System.exit(1)
  }
  val bw = outputfw.get

  val result = processInputFile(inputLogFile.getLines().drop(1), Result(Map(), Map()), 0)

  // print all ips that have not been resolved yet
  val orderedIps = result.ipMap.map(x => (x._1, x._2.head.order)).toList
  orderedIps.sortBy(_._2).foreach(o => {
    writeOutputToFile(createOutput(result.ipMap.get(o._1).get, o._1))
  })

  Try(inputLogFile.close)
  Try(bw.close)

  def usage(message: String): Unit = {
    println(message)
    println("Usage: EdgarLogProcessor {path to log file} {path to inactiviy_period file} {path to output file}")
    System.exit(1)
  }

  def getLine(line: String, ipMap: Map[String, Set[InputLine]], order: Int): InputLine = {
    val words = line.split(',')
    // following is to keep track of the chronological order
    val o = ipMap.get(words(0)) match {
      case None => order
      case Some(x) => x.head.order
    }

    InputLine(words(0), dateFormat.parse(s"${words(1)} ${words(2)}"), words(4), words(5), words(6), o)
  }

  /*
  The main function where it all starts
   */
  @tailrec
  def processInputFile(iter: Iterator[String], result: Result, order: Int): Result = {
    if (iter.hasNext) {
      val readLine = Try(getLine(iter.next(), result.ipMap, order))
      if (readLine.isFailure) {
        println("There was a failure to read the input file, discontinuing")
        result
      } else {
        val line = readLine.get
        val resultAfterClosingSessions = closeExpiredSessions(line, result)
        processInputFile(iter, processLine(line, resultAfterClosingSessions), order + 1)
      }
    } else
      result
  }

  def closeExpiredSessions(line: InputLine, result: Result): Result = {
    val currentTime = line.dt.getTime

    // get all open sessions that are set to expire before now and create output records for them
    val (expiredSessions, rest) = result.expireMap.partition({ case (k, _) => (k < currentTime) })
    val output = expiredSessions.keys.toList.sorted.collect({ case t =>
      for (r <- expiredSessions.get(t).get; ipset = result.ipMap.get(r) if (ipset.isDefined))
        yield createOutput(ipset.get, r)
    }).flatten

    // remove the ips of the expired sessions from our internal map
    val ipMap = result.ipMap.foldLeft(Map[String, Set[InputLine]]())({
      case (a: Map[String, Set[InputLine]], (k: String, v: Set[InputLine])) =>
        if (!(output.exists(_.ip == k))) a + (k -> v) else a
    })

    // write details of the expired sessions
    output.foreach(writeOutputToFile)

    Result(ipMap, rest)
  }

  // Process next line
  def processLine(line: InputLine, result: Result): Result = {
    // Manage internal maps with new line's information
    // if it is a new IP, add it to the ipMap and set it to expire at appropriate time
    // if it is a known IP, add it to the ipMap and change the expire time appropriately
    result.ipMap.get(line.ip) match {
      case None => {
        Result(result.ipMap + (line.ip -> Set(line)), addToExpireMap(line, result.expireMap))
      }
      case Some(x) => {
        val maxtime = x.maxBy(_.dt.getTime).dt.getTime
        if (maxtime == line.dt.getTime)
          Result(result.ipMap + (line.ip -> (x + line)), result.expireMap)
        else Result(result.ipMap + (line.ip -> (x + line)), changeExpireMap(line, result.expireMap, maxtime))
      }
    }
  }

  // utility function to change map tracking expiration times
  def changeExpireMap(line: InputLine, expireMap: Map[Long, List[String]], maxtime: Long): Map[Long, List[String]] = {
    val key = maxtime + expireMillis
    removeFromExpireMap(key, line, addToExpireMap(line, expireMap))
  }

  // utility function
  def addToExpireMap(line: InputLine, expireMap: Map[Long, List[String]]): Map[Long, List[String]] = {
    val expireTime = line.dt.getTime + expireMillis
    expireMap.get(expireTime) match {
      case Some(x) => expireMap + (expireTime -> (x :+ line.ip))
      case None => expireMap + (expireTime -> List(line.ip))
    }
  }

  def removeFromExpireMap(at: Long, line: InputLine, expireMap: Map[Long, List[String]]): Map[Long, List[String]] = {
    val atSet = expireMap.get(at).getOrElse(Set())
    expireMap + ((at) -> atSet.filter(_ != line.ip).toList)
  }

  def createOutput(ipset: Set[InputLine], ip: String): Output = {
    val start = ipset.minBy(_.dt.getTime).dt
    val end = ipset.maxBy(_.dt.getTime).dt
    Output(ip, start, end, ipset.size)
  }

  def writeOutputToFile(row: Output) = {
    val message = s"${row.ip}," +
      s"${dateFormat.format(row.start)}," +
      s"${dateFormat.format(row.end)}," +
      s"${(row.end.getTime - row.start.getTime) / 1000 + 1}," +
      s"${row.numDocs}\n"

    def writeAndFlush = {
      bw.write(message)
      bw.flush()
    }
    val written = Try(writeAndFlush)

    if (written.isFailure) {
      println("There was a failure to write the following message to output file:")
      print(message)
    }
  }

}
