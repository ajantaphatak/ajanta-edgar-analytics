# Background
Although, Scala is not my strongest language, I decided to write this code in Scala because I am learning it. 
For the first version, I used mutable maps but then decided to go with the functional
approach of immutability and am pretty happy with the results, except for one thing - 
line 148 in the function addToExpireMap - appending to the list. I understand this may not be the most optimal way but just prepending was not possible
as the order is important in this case. That would be one place where it may be a potential
to optimize using a ListBuffer to build the list and then convert it finally to a List. 

I did not get a chance to test this on large input but feel and hope it should hold up pretty well.

# Dependencies
Scala 2.11.11 and jdk1.8.0_151 were used while developing this code and run.sh assumes these are available in the PATH.

# Approach
The code reads the file line by line and processes each line. It uses 2 main data structures, both maps - ipMap and expireMap.

ipMap keeps track of all the lines seen for an IP address until they are ready to expire.

expireMap keeps track of the times when a session for an IP should be closed.

The algorithm follows the following steps for each line that is read: 

* When reading a line from the file, we need to keep track of order of when an IP is seen.
 If an IP has been seen before (from remaining open sessions) we need to note that order.

* After reading a line, the first thing to check is if there are any sessions
 that can be closed. The code in the method closeExpireSessions checks that and 
 closes any open sessions. When closing sessions, it needs to remove the IPs,
 for which the session is being closed, from the ipMap. It also writes the closed sessions
 to the output file.
 
* After the past sessions are closed, processing begins for the next line read. The processing
 consists of adding the line to the internal maps. Adding it to the ipMap and adjusting the 
 expireMap depending on whether this IP was seen before.

* At the end, the remaining entries in the ipMap need to be written to the output file.
 Here we need to make sure the order of the output matches the order of when the IPs appeared
 in the input file.
  
* Finally, all the files are closed.
